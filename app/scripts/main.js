
var lives = 0;
var coins = 2;
var stopped = true;
var data = null; 

var pfx = ["webkit", "moz", "MS", "o", ""];
function PrefixedEvent(element, type, callback) {
    for (var p = 0; p < pfx.length; p++) {
        if (!pfx[p]) type = type.toLowerCase();
        element.addEventListener(pfx[p]+type, callback, false);
    }
}
$(function () {
  console.log("cat");
  $(".coins").text(coins);
  $(".extralife").text(lives);
});
$("body").keypress(function (e) {
  if (stopped && coins > 0) {
    for (var i = 1; i <= 3; i++) {
      $('.slot' + i).removeClass("transto1");
      $('.slot' + i).removeClass("transto2");
      $('.slot' + i).removeClass("transto3");
      $('.slot' + i).removeClass("transto0");
    }
    stopped = false;
    var key = e.keyCode ? e.keyCode : e.which;
    if (key == 97) {
      $(".start").get(0).play();
      Mediator.publish('inputGiven');
      for (var i = 1; i <= 3; i++) {
        var elem = $(".slot" + i); 
          elem.addClass('rotating');
          //console.log(elem);
          //PrefixedEvent(elem[0], "AnimationIteration", function(e){
          //  console.log("i has a call");
          //});

      }
    }
  }
});
var Mediator = (function () {

    var channels = {};

    //
    // Subscribe to a channel
    //
    function subscribe(channel, fn, that) {
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({
            context: that,
            callback: fn
        });
    }

    //
    // Publish a channel (like triggering an event)
    //
    function publish(channel) {

        if (!channels[channel]) {
            return false;
        }
        var args = Array.prototype.slice.call(arguments, 1);
        for (var i = 0, l = channels[channel].length; i < l; i += 1) {
            var subscription = channels[channel][i];
            if (typeof subscription.context === undefined) {
                subscription.callback(args);
            } else {
                subscription.callback.call(subscription.context, args);
            }
        }
    }

    return {
        subscribe: subscribe,
        publish: publish
    }

})();


var BackendConnector = (function () {
    function send() {
        var xhr = $.ajax({
            url: "http://fettblog.eu/fh/numbers.php?jsonp",
            dataType: 'jsonp',
            success: function (data) {
                Mediator.publish('datareceived',data);
            },
            error: function (err) {

            }
        });

        setTimeout(function(){
        if(xhr.status != 200){
            xhr.fail(); 
            alert("i has a timeout");
        }

    }, 500);

    }

    Mediator.subscribe('inputGiven', send);

})();





Mediator.subscribe('datareceived', function (d) {
    console.log('i has data' + d[0].slots + " and " + d[0].result);
  stopped = true; 
  for (var i = 0; i < 3; i++) {
     $('.slot' + (i + 1)).removeClass('rotating');
     $('.slot' + (i + 1)).addClass('transto' + d[0].slots[i]);
     }
     var result = parseInt(d[0].result); 
     if (result > 0){
          $(".win").get(0).play();
              lives = lives + result;
              coins--;
            } else {
              $(".lose").get(0).play();
              coins--;
            }
     $(".coins").text(coins);
            $(".extralife").text(lives);
  });